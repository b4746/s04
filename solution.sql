-- ACTIVITY



-- a. Find all artist that has leytter D in its name.
		Answer:

			SELECT * FROM songs WHERE song_name LIKE "%D%";

-- b. Find all song that has a length of less than 230 minutes.
		Answer:
			
			SELECT song_name FROM tbl_songs WHERE length < 230;	

-- c. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
		Answer:

			SELECT albums.album_title, tbl_songs.song_name, tbl_songs.length FROM albums 
				JOIN tbl_songs ON albums.id = tbl_songs.album_id;
			
-- d. Joim the 'artists' and 'albums' tables. (Find all albums that has letter A in its name.)
		Answer:

			SELECT * FROM artists 
				JOIN albums ON artists.id = albums.artist_id WHERE name LIKE '%a%';

-- e. Sort the albums in Z-A order. (Show only the first 4 records.)
		Answer: 

			SELECT album_title FROM albums ORDER BY album_title DESC LIMIT 4;

-- f. Join the 'albums' and 'songs' tables. (Sort albums from Z-A and sort songs from A-Z)
		Answer:


			SELECT albums.album_title, tbl_songs.song_name FROM albums 
				JOIN tbl_songs ORDER BY DESC;
			
			SELECT albums.album_title, tbl_songs.song_name FROM tbl_songs 
				JOIN albums ORDER BY ASC;
